package com.koenvdels.ordersystem.api.impl;

import com.koenvdels.ordersystem.api.ClientService;
import com.koenvdels.ordersystem.api.inputTypes.ClientInput;
import com.koenvdels.ordersystem.api.models.Client;
import com.koenvdels.ordersystem.configs.Roles;
import com.koenvdels.ordersystem.dataModels.ClientData;
import com.koenvdels.ordersystem.repositories.ClientRepository;
import com.koenvdels.ordersystem.services.Context;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.naming.AuthenticationException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ClientServiceImpl implements ClientService {

    @Autowired
    private ClientRepository repo;
    @Autowired
    private Context context;

    @Override
    public List<Client> findAll() throws AuthenticationException {
        if(!context.hasRole(Roles.ADMIN)) throw new AuthenticationException("Unauthorized ");
        return repo.findAll().stream().map(data -> new ClientImpl(data, context)).collect(Collectors.toList());
    }

    @Override
    public Optional<Client> findOne(long id) throws AuthenticationException {
        if(!context.hasRole(Roles.ADMIN)) throw new AuthenticationException("Unauthorized");
        Optional<ClientData> client = repo.findById(id);
        return client.map(data -> new ClientImpl(data, context));
    }

    @Override
    public Client createClient(ClientInput clientInput) throws AuthenticationException {
        ClientImpl client = new ClientImpl(new ClientData(), context);
        client.setName(clientInput.name);
        client.setPhoneNumber(clientInput.phoneNumber);
        client.setAddress(clientInput.address);
        client.setEmail(clientInput.email);
        repo.save(client.getData());
        return client;
    }

    @Override
    public Optional<Client> patchClient(long clientId, ClientInput input) throws AuthenticationException {
        if(!context.hasRole(Roles.ADMIN)) throw new AuthenticationException("Unauthorized ");
        Optional<Client> optional = findOne(clientId);
        if (optional.isEmpty()) return Optional.empty();

        Client client = optional.get();

        client.setName(input.name == null ? client.getName() : input.name);
        client.setPhoneNumber(input.phoneNumber == null ? client.getPhoneNumber() : input.phoneNumber);
        client.setAddress(input.address == null ? client.getAddress() : input.address);
        client.setEmail(input.email == null ? client.getEmail() : input.email);

        return Optional.of(client);
    }

    @Override
    public boolean deleteClient(long clientId) throws AuthenticationException {
        if(!context.hasRole(Roles.ADMIN)) throw new AuthenticationException("Unauthorized ");
        Optional<Client> optional = findOne(clientId);
        if (optional.isEmpty()) return false;

        ClientImpl client = (ClientImpl) optional.get();

        repo.delete(client.getData());
        return true;
    }
}
