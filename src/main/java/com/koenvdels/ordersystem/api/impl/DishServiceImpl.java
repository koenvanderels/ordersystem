package com.koenvdels.ordersystem.api.impl;

import com.koenvdels.ordersystem.api.DishService;
import com.koenvdels.ordersystem.api.exceptions.UnauthorizedException;
import com.koenvdels.ordersystem.api.inputTypes.DishInput;
import com.koenvdels.ordersystem.api.models.Dish;
import com.koenvdels.ordersystem.dataModels.DishData;
import com.koenvdels.ordersystem.repositories.DishRepository;
import com.koenvdels.ordersystem.services.Context;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class DishServiceImpl implements DishService {

    private final DishRepository dishRepository;
    private final Context context;

    public DishServiceImpl(DishRepository dishService, Context context) {
        this.dishRepository = dishService;
        this.context = context;
    }

    @Override
    public List<Dish> findAll() {
        return dishRepository.findAll().stream().map(data -> new DishImpl(data, context)).collect(Collectors.toList());
    }

    @Override
    public Dish findOne(long id) {
        return new DishImpl(dishRepository.getOne(id), context);
    }

    @Override
    public Dish createDish(DishInput dishInput) throws UnauthorizedException {
        DishImpl dish = new DishImpl(new DishData(), context);

        dish.setName(dishInput.name);
        dish.setPrice(dishInput.price);

        dishRepository.save(dish.getData());
        return dish;
    }

    @Override
    public Optional<Dish> findById(long id) {
        Optional<DishData> dish = dishRepository.findById(id);
        return dish.map(data -> new DishImpl(data, context));

    }

    @Override
    @Transactional
    public Optional<Dish> patchDish(long id, DishInput input) throws UnauthorizedException {

        Optional<Dish> optional = findById(id);
        if (optional.isEmpty()) return Optional.empty();

        Dish dish = optional.get();
        dish.setName(input.name.equals("") ? dish.getName() : input.name);
        dish.setPrice(input.price == 0 ? dish.getPrice() : input.price);

        return Optional.of(dish);
    }

    @Override
    public boolean deleteDish(long dishId) throws UnauthorizedException {
        Optional<Dish> optional = findById(dishId);
        if (optional.isEmpty()) return false;

        DishImpl dish = (DishImpl) optional.get();

        dishRepository.delete(dish.getData());
        return true;
    }

}
