package com.koenvdels.ordersystem.api.impl;

import com.koenvdels.ordersystem.api.DishService;
import com.koenvdels.ordersystem.api.OrderService;
import com.koenvdels.ordersystem.api.OrderedItemService;
import com.koenvdels.ordersystem.api.exceptions.UnauthorizedException;
import com.koenvdels.ordersystem.api.inputTypes.OrderedItemInput;
import com.koenvdels.ordersystem.api.models.Dish;
import com.koenvdels.ordersystem.api.models.Order;
import com.koenvdels.ordersystem.api.models.OrderedItem;
import com.koenvdels.ordersystem.configs.Roles;
import com.koenvdels.ordersystem.dataModels.OrderedItemsData;
import com.koenvdels.ordersystem.repositories.OrderedItemsRepository;
import com.koenvdels.ordersystem.services.Context;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class OrderedItemServiceImpl implements OrderedItemService {

    private final OrderedItemsRepository repository;
    private final DishService dishService;
    private final OrderService orderService;
    private final Context context;

    public OrderedItemServiceImpl(OrderedItemsRepository repository, DishService dishService, OrderService orderService, Context context) {
        this.repository = repository;
        this.dishService = dishService;
        this.orderService = orderService;
        this.context = context;
    }

    @Override
    public OrderedItem addItemToOrder(OrderedItemInput input) throws UnauthorizedException {
        Dish dish = dishService.findById(Long.parseLong(input.dishId)).orElseThrow();
        Order order = orderService.findOne(Long.parseLong(input.orderId)).orElseThrow();

        OrderedItemImpl orderedItem = new OrderedItemImpl(new OrderedItemsData(), context);
        orderedItem.setAmount(input.amount);
        orderedItem.setDish(dish);
        orderedItem.setOrder(order);
        orderedItem.setPrice(dish.getPrice() * input.amount);

        repository.save(orderedItem.getData());
        return orderedItem;
    }

    @Override
    public Optional<OrderedItem> patchOrderedItem(long orderedItemId, OrderedItemInput input) throws UnauthorizedException {
        if (!context.hasRole(Roles.ADMIN)) throw new UnauthorizedException("Unauthorized");

        Optional<OrderedItem> optional = findById(orderedItemId);
        if (optional.isEmpty()) return Optional.empty();

        OrderedItem item = optional.get();
        item.setAmount(input.amount != 0 ? input.amount : item.getAmount());

        return Optional.of(item);
    }

    @Override
    public Optional<OrderedItem> findById(long id) {
        return repository.findById(id).map(data -> new OrderedItemImpl(data, context));
    }

    @Override
    public boolean removeOrderedItem(long orderedItemId) {
        Optional<OrderedItem> optional = findById(orderedItemId);
        if (optional.isEmpty()) return false;

        OrderedItemImpl item = (OrderedItemImpl) optional.get();
        repository.delete(item.getData());
        return true;
    }
}
