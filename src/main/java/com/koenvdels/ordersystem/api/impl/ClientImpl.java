package com.koenvdels.ordersystem.api.impl;

import com.koenvdels.ordersystem.api.models.Client;
import com.koenvdels.ordersystem.api.models.Order;
import com.koenvdels.ordersystem.configs.Roles;
import com.koenvdels.ordersystem.dataModels.ClientData;
import com.koenvdels.ordersystem.services.Context;

import javax.naming.AuthenticationException;

public class ClientImpl implements Client {

    private final ClientData data;
    private final Context context;

    public ClientImpl(ClientData data, Context context) {
        this.data = data;
        this.context = context;
    }

    @Override
    public long getId() throws AuthenticationException {
        if(!context.hasRole(Roles.ADMIN)) throw new AuthenticationException("Unauthorized");
        return data.getId();
    }

    @Override
    public String getName() throws AuthenticationException {
        if(!context.hasRole(Roles.ADMIN)) throw new AuthenticationException("Unauthorized");
        return data.getName();
    }

    @Override
    public void setName(String name) throws AuthenticationException {
        if(!context.hasRole(Roles.ADMIN)) throw new AuthenticationException("Unauthorized");
        data.setName(name);
    }

    @Override
    public String getPhoneNumber() throws AuthenticationException {
        if(!context.hasRole(Roles.ADMIN)) throw new AuthenticationException("Unauthorized");
        return data.getPhoneNumber();
    }

    @Override
    public void setPhoneNumber(String phoneNumber) throws AuthenticationException {
        if(!context.hasRole(Roles.ADMIN)) throw new AuthenticationException("Unauthorized");
        data.setPhoneNumber(phoneNumber);
    }

    @Override
    public String getEmail() throws AuthenticationException {
        if(!context.hasRole(Roles.ADMIN)) throw new AuthenticationException("Unauthorized");
        return data.getEmail();
    }

    @Override
    public void setEmail(String email) throws AuthenticationException {
        if(!context.hasRole(Roles.ADMIN)) throw new AuthenticationException("Unauthorized");
        data.setEmail(email);
    }

    @Override
    public String getAddress() throws AuthenticationException {
        if(!context.hasRole(Roles.ADMIN)) throw new AuthenticationException("Unauthorized");
        return data.getAddress();
    }

    @Override
    public void setAddress(String address) throws AuthenticationException {
        if(!context.hasRole(Roles.ADMIN)) throw new AuthenticationException("Unauthorized");
        data.setAddress(address);
    }

    @Override
    public Order getOrder() throws Exception {
        if(!context.hasRole(Roles.ADMIN)) throw new Exception("You are not allowed to get this resource");
        return new OrderImpl(data.getOrderData(), context);
    }

    public ClientData getData() {
        return data;
    }
}
