package com.koenvdels.ordersystem.api.impl;

import com.koenvdels.ordersystem.api.exceptions.UnauthorizedException;
import com.koenvdels.ordersystem.api.models.Dish;
import com.koenvdels.ordersystem.api.models.OrderedItem;
import com.koenvdels.ordersystem.configs.Roles;
import com.koenvdels.ordersystem.dataModels.DishData;
import com.koenvdels.ordersystem.services.Context;

import java.util.List;
import java.util.stream.Collectors;

public class DishImpl implements Dish {

    private final Context context;
    private final DishData data;

    public DishImpl(DishData data, Context context) {
        this.data = data;
        this.context = context;
    }

    @Override
    public long getId() {
        return data.getId();
    }

    @Override
    public String getName() {
        return data.getName();
    }

    @Override
    public void setName(String name) throws UnauthorizedException {
        if (!context.hasRole(Roles.ADMIN)) throw new UnauthorizedException("Your not allowed to execute this method");
        data.setName(name);
    }

    @Override
    public int getPrice() {
        return data.getPrice() / 100;
    }

    @Override
    public void setPrice(int price) throws UnauthorizedException {
        if (!context.hasRole(Roles.ADMIN)) throw new UnauthorizedException("Your not allowed to execute this method");
        data.setPrice(price);
    }

    @Override
    public List<OrderedItem> getOrderedItems() throws UnauthorizedException {
        if (!context.hasRole(Roles.ADMIN)) throw new UnauthorizedException("Your not allowed to execute this method");
        return data.getOrderedItemData().stream().map(data -> new OrderedItemImpl(data, context)).collect(Collectors.toList());
    }

    public DishData getData() {
        return data;
    }
}
