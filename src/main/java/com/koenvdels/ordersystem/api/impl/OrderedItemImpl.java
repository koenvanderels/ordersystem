package com.koenvdels.ordersystem.api.impl;

import com.koenvdels.ordersystem.api.exceptions.UnauthorizedException;
import com.koenvdels.ordersystem.api.models.Dish;
import com.koenvdels.ordersystem.api.models.Order;
import com.koenvdels.ordersystem.api.models.OrderedItem;
import com.koenvdels.ordersystem.configs.Roles;
import com.koenvdels.ordersystem.dataModels.OrderedItemsData;
import com.koenvdels.ordersystem.services.Context;

public class OrderedItemImpl implements OrderedItem {

    private OrderedItemsData data;
    private Context context;

    public OrderedItemImpl(OrderedItemsData data, Context context) {
        this.data = data;
        this.context = context;
    }

    @Override
    public long getId() {
        return data.getId();
    }

    @Override
    public Dish getDish() {
        return new DishImpl(data.getDishData(), context);
    }

    @Override
    public void setDish(Dish dish) {
        DishImpl dishImpl = (DishImpl) dish;
        data.setDishData(dishImpl.getData());
    }

    @Override
    public int getAmount() {
        return data.getAmount();
    }

    @Override
    public void setAmount(int amount) {
        data.setAmount(amount);
    }

    @Override
    public double getPrice() {
        return data.getAmount() * data.getDishData().getPrice();
    }

    @Override
    public void setPrice(double price) throws UnauthorizedException {
        if(!context.hasRole(Roles.ADMIN)) throw new UnauthorizedException("Unauthorized");
        data.setPrice(price);
    }

    @Override
    public void setOrder(Order order) throws UnauthorizedException {
        if(!context.hasRole(Roles.ADMIN)) throw new UnauthorizedException("Unauthorized");
        OrderImpl orderImpl = (OrderImpl) order;
        data.setOrderData(orderImpl.getOrderData());
    }

    public OrderedItemsData getData() {
        return data;
    }
}
