package com.koenvdels.ordersystem.api.impl;

import com.koenvdels.ordersystem.api.OrderService;
import com.koenvdels.ordersystem.api.exceptions.UnauthorizedException;
import com.koenvdels.ordersystem.api.inputTypes.OrderInput;
import com.koenvdels.ordersystem.api.models.Order;
import com.koenvdels.ordersystem.configs.Roles;
import com.koenvdels.ordersystem.dataModels.OrderData;
import com.koenvdels.ordersystem.repositories.OrderRepository;
import com.koenvdels.ordersystem.services.Context;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class OrderServiceImpl implements OrderService {

    private final OrderRepository repository;
    private final Context context;

    public OrderServiceImpl(OrderRepository repository, Context context) {
        this.repository = repository;
        this.context = context;
    }

    @Override
    public Optional<Order> findOne(long id) throws UnauthorizedException {
        if(!context.hasRole(Roles.ADMIN)) throw new UnauthorizedException("Unauthorized");
        return repository.findById(id).map(data -> new OrderImpl(data, context));
    }

    @Override
    public List<Order> findAll() throws UnauthorizedException {
        if(!context.hasRole(Roles.ADMIN)) throw new UnauthorizedException("Unauthorized");
        return repository.findAll().stream().map(data -> new OrderImpl(data, context))
                .collect(Collectors.toList());
    }

    @Override
    public Order createOrder(OrderInput orderInput) {
        OrderImpl order = new OrderImpl(new OrderData(), context);
        order.setIsPaid(orderInput.paid);
        order.setClient(orderInput.client);
        repository.save(order.getOrderData());
        return order;
    }

    @Override
    public boolean removeOrder(long orderId) throws UnauthorizedException {
        if(!context.hasRole(Roles.ADMIN)) throw new UnauthorizedException("Unauthorized");
        Optional<Order> optional = findOne(orderId);
        if (optional.isEmpty()) return false;

        OrderImpl order = (OrderImpl) optional.get();
        repository.delete(order.getOrderData());
        return true;

    }
}
