package com.koenvdels.ordersystem.api.impl;

import com.koenvdels.ordersystem.api.models.Client;
import com.koenvdels.ordersystem.api.models.Dish;
import com.koenvdels.ordersystem.api.models.Order;
import com.koenvdels.ordersystem.api.models.OrderedItem;
import com.koenvdels.ordersystem.dataModels.ClientData;
import com.koenvdels.ordersystem.dataModels.OrderData;
import com.koenvdels.ordersystem.dataModels.OrderedItemsData;
import com.koenvdels.ordersystem.services.Context;

import java.util.List;
import java.util.stream.Collectors;

public class OrderImpl implements Order {

    private OrderData data;
    private Context context;

    public OrderImpl(OrderData data, Context context) {
        this.data = data;
        this.context = context;
    }

    @Override
    public long getId() {
        return data.getId();
    }

    @Override
    public void addDish(Dish dish, int amount) {
        DishImpl dishImpl = (DishImpl) dish;

        OrderedItemsData orderedItem = new OrderedItemsData();
        orderedItem.setAmount(amount);
        orderedItem.setDishData(dishImpl.getData());
        orderedItem.setPrice(dishImpl.getPrice());

        data.getOrderedItemData().add(orderedItem);
    }

    @Override
    public List<OrderedItem> getOrderedItems() {
        return data.getOrderedItemData().stream().map(data -> new OrderedItemImpl(data, context)).collect(Collectors.toList());
    }

    @Override
    public Client getClient() {
        return new ClientImpl(data.getClientData(), context);
    }

    @Override
    public void setClient(Client client) {
        ClientData clientData = ((ClientImpl) client).getData();
        data.setClientData(clientData);
    }

    @Override
    public boolean isPaid() {
        return data.isPaid();
    }

    @Override
    public void setIsPaid(boolean isPaid) {
        data.setPaid(isPaid);
    }

    public OrderData getOrderData() {
        return data;
    }
}
