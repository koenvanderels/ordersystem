package com.koenvdels.ordersystem.api.models;

import java.util.List;

public interface Order {

    long getId();

    void addDish(Dish dish, int amount);

    List<OrderedItem> getOrderedItems();

    Client getClient();

    void setClient(Client client);

    boolean isPaid();

    void setIsPaid(boolean isPaid);
}
