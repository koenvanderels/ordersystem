package com.koenvdels.ordersystem.api.models;

import com.koenvdels.ordersystem.api.exceptions.UnauthorizedException;

public interface OrderedItem {

    long getId();

    Dish getDish();

    void setDish(Dish dish) throws UnauthorizedException;

    int getAmount();

    void setAmount(int amount) throws UnauthorizedException;

    double getPrice();

    void setPrice(double price) throws UnauthorizedException;

    void setOrder(Order order) throws UnauthorizedException;

}
