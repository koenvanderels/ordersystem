package com.koenvdels.ordersystem.api.models;

import javax.naming.AuthenticationException;

public interface Client {
    long getId() throws AuthenticationException;

    String getName() throws AuthenticationException;

    void setName(String name) throws AuthenticationException;

    String getPhoneNumber() throws AuthenticationException;

    void setPhoneNumber(String phoneNumber) throws AuthenticationException;

    String getEmail() throws AuthenticationException;

    void setEmail(String email) throws AuthenticationException;

    String getAddress() throws AuthenticationException;

    void setAddress(String address) throws AuthenticationException;

    Order getOrder() throws Exception;
}
