package com.koenvdels.ordersystem.api.models;

import com.koenvdels.ordersystem.api.exceptions.UnauthorizedException;

import java.util.List;

public interface Dish {

    long getId();

    String getName();

    void setName(String name) throws UnauthorizedException;

    int getPrice();

    void setPrice(int price) throws UnauthorizedException;

    List<OrderedItem> getOrderedItems() throws UnauthorizedException;
}
