package com.koenvdels.ordersystem.api;

import com.koenvdels.ordersystem.api.inputTypes.ClientInput;
import com.koenvdels.ordersystem.api.models.Client;
import org.springframework.security.access.prepost.PreAuthorize;

import javax.naming.AuthenticationException;
import java.util.List;
import java.util.Optional;

public interface ClientService {

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    List<Client> findAll() throws AuthenticationException;

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    Optional<Client> findOne(long id) throws AuthenticationException;


    /**
     * Creates and saves the Entity
     *
     * @param clientInput plain object that contains the necessary fields
     * @return the new Client Object
     */
    Client createClient(ClientInput clientInput) throws AuthenticationException;

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    Optional<Client> patchClient(long clientId, ClientInput clientInput) throws AuthenticationException;

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    boolean deleteClient(long clientId) throws AuthenticationException;
}
