package com.koenvdels.ordersystem.api;

import com.koenvdels.ordersystem.api.exceptions.UnauthorizedException;
import com.koenvdels.ordersystem.api.inputTypes.DishInput;
import com.koenvdels.ordersystem.api.models.Dish;
import org.springframework.security.access.prepost.PreAuthorize;

import javax.naming.AuthenticationException;
import java.util.List;
import java.util.Optional;

public interface DishService {


    List<Dish> findAll();

    @Deprecated
    Dish findOne(long id);

    Dish createDish(DishInput dishInput) throws UnauthorizedException, AuthenticationException;

    Optional<Dish> findById(long id);

    Optional<Dish> patchDish(long id, DishInput dishInput) throws UnauthorizedException;

    /**
     * @param dishId The Id of the dish
     * @return True if the resource was deleted, false otherwise
     */
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    boolean deleteDish(long dishId) throws UnauthorizedException;
}
