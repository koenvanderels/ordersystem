package com.koenvdels.ordersystem.api.inputTypes;

public class ClientInput {

    public String name;
    public String phoneNumber;
    public String email;
    public String address;

}
