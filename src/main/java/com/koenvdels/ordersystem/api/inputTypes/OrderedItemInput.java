package com.koenvdels.ordersystem.api.inputTypes;

public class OrderedItemInput {
    public String dishId;
    public String orderId;
    public int amount;
}
