package com.koenvdels.ordersystem.api.inputTypes;

import com.koenvdels.ordersystem.api.models.Client;

public class OrderInput {

    public boolean paid;
    public Client client;

}
