package com.koenvdels.ordersystem.api;

import com.koenvdels.ordersystem.api.exceptions.UnauthorizedException;
import com.koenvdels.ordersystem.api.inputTypes.OrderInput;
import com.koenvdels.ordersystem.api.models.Order;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.List;
import java.util.Optional;

public interface OrderService {
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    Optional<Order> findOne(long id) throws UnauthorizedException;

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    List<Order> findAll() throws UnauthorizedException;

    /**
     * The creates and saves the newly created entity to the database
     *
     * @return the created order
     */
    Order createOrder(OrderInput orderInput);

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    boolean removeOrder(long orderId) throws UnauthorizedException;
}
