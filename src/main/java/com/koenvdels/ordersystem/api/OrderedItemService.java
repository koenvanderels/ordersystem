package com.koenvdels.ordersystem.api;

import com.koenvdels.ordersystem.api.exceptions.UnauthorizedException;
import com.koenvdels.ordersystem.api.inputTypes.OrderedItemInput;
import com.koenvdels.ordersystem.api.models.OrderedItem;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.Optional;

public interface OrderedItemService {

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    OrderedItem addItemToOrder(OrderedItemInput orderedItemInput) throws UnauthorizedException;

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    Optional<OrderedItem> patchOrderedItem(long orderedItemId, OrderedItemInput input) throws UnauthorizedException;

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    Optional<OrderedItem> findById(long id);

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    boolean removeOrderedItem(long orderedItemId);
}
