package com.koenvdels.ordersystem.dataModels;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "dishes")
public class DishData {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String name;
    private int price;

    @OneToMany(mappedBy = "dishData")
    private List<OrderedItemsData> orderedItemData;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public List<OrderedItemsData> getOrderedItemData() {
        return orderedItemData;
    }

    public void setOrderedItemData(List<OrderedItemsData> orderedItemData) {
        this.orderedItemData = orderedItemData;
    }
}
