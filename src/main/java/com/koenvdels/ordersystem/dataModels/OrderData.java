package com.koenvdels.ordersystem.dataModels;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "orders")
public class OrderData {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @OneToMany(mappedBy = "orderData", cascade = CascadeType.PERSIST)
    private List<OrderedItemsData> orderedItemData = new ArrayList<>();

    @OneToOne(mappedBy = "orderData")
    private ClientData clientData;

    private boolean paid;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<OrderedItemsData> getOrderedItemData() {
        return orderedItemData;
    }

    public void setOrderedItemData(List<OrderedItemsData> orderedItemData) {
        this.orderedItemData = orderedItemData;
    }

    public ClientData getClientData() {
        return clientData;
    }

    public void setClientData(ClientData clientData) {
        this.clientData = clientData;
    }

    public boolean isPaid() {
        return paid;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }
}
