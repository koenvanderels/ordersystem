package com.koenvdels.ordersystem.dataModels;

import javax.persistence.*;

@Entity
@Table(name = "order_items")
public class OrderedItemsData {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    private DishData dishData;

    @ManyToOne
    private OrderData orderData;

    private int amount;
    private double price;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public DishData getDishData() {
        return dishData;
    }

    public void setDishData(DishData dishData) {
        this.dishData = dishData;
    }

    public OrderData getOrderData() {
        return orderData;
    }

    public void setOrderData(OrderData orderData) {
        this.orderData = orderData;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
