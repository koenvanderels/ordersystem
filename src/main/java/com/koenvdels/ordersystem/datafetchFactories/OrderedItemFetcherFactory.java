package com.koenvdels.ordersystem.datafetchFactories;

import com.koenvdels.ordersystem.api.OrderedItemService;
import com.koenvdels.ordersystem.api.inputTypes.OrderedItemInput;
import com.koenvdels.ordersystem.api.models.OrderedItem;
import com.koenvdels.ordersystem.services.GraphQLInputConverter;
import graphql.schema.DataFetcher;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;

@Component
public class OrderedItemFetcherFactory {

    private final OrderedItemService orderedItemService;
    private final GraphQLInputConverter converter;

    public OrderedItemFetcherFactory(OrderedItemService orderedItemService, GraphQLInputConverter converter) {
        this.orderedItemService = orderedItemService;
        this.converter = converter;
    }

    public DataFetcher<OrderedItem> getAddOrderedItem() {
        return env -> {
            OrderedItemInput input = converter.convert(env.getArgument("OrderedItem"), OrderedItemInput.class);
            return orderedItemService.addItemToOrder(input);
        };
    }

    public DataFetcher<OrderedItem> getEditOrderedItem() {
        return env -> {
            long orderedItemId = Long.parseLong(env.getArgument("orderedItemId"));
            OrderedItemInput input = converter.convert(env.getArgument("OrderedItem"), OrderedItemInput.class);
            return orderedItemService.patchOrderedItem(orderedItemId, input).orElseThrow(EntityNotFoundException::new);
        };
    }

    public DataFetcher<Boolean> getRemoveOrderedItem() {
        return env -> {
            long orderedItemId = Long.parseLong(env.getArgument("orderedItemId"));
            return orderedItemService.removeOrderedItem(orderedItemId);
        };
    }
}
