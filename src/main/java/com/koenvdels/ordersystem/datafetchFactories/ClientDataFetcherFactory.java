package com.koenvdels.ordersystem.datafetchFactories;

import com.koenvdels.ordersystem.api.ClientService;
import com.koenvdels.ordersystem.api.inputTypes.ClientInput;
import com.koenvdels.ordersystem.api.models.Client;
import com.koenvdels.ordersystem.services.GraphQLInputConverter;
import graphql.schema.DataFetcher;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ClientDataFetcherFactory {

    private final ClientService clientService;
    private final GraphQLInputConverter converter;

    public ClientDataFetcherFactory(ClientService clientService, GraphQLInputConverter converter) {
        this.clientService = clientService;
        this.converter = converter;
    }

    public DataFetcher<Client> getClientDataFetcher() {
        return env -> clientService.findOne(Long.parseLong(env.getArgument("id"))).orElse(null);
    }

    public DataFetcher<List<Client>> getAllClientDataFetcher() {
        return env -> clientService.findAll();
    }

    public DataFetcher<Client> getEditClient() {
        return env -> {
            long clientId = Long.parseLong(env.getArgument("clientId"));
            ClientInput input = converter.convert(env.getArgument("clientInput"), ClientInput.class);
            return clientService.patchClient(clientId, input).orElse(null);
        };
    }

    public DataFetcher<Boolean> getRemoveClient() {
        return env -> {
            long clientId = Long.parseLong(env.getArgument("clientId"));
            return clientService.deleteClient(clientId);
        };
    }
}
