package com.koenvdels.ordersystem.datafetchFactories;

import com.koenvdels.ordersystem.api.DishService;
import com.koenvdels.ordersystem.api.inputTypes.DishInput;
import com.koenvdels.ordersystem.api.models.Dish;
import com.koenvdels.ordersystem.services.GraphQLInputConverter;
import graphql.schema.DataFetcher;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Component
public class DishDataFetcherFactory {

    private final DishService dishService;
    private final GraphQLInputConverter converter;

    public DishDataFetcherFactory(DishService dishRepository, GraphQLInputConverter converter) {
        this.dishService = dishRepository;
        this.converter = converter;
    }

    public DataFetcher<List<Dish>> getAllDishes() {
        return env -> dishService.findAll();
    }

    public DataFetcher<Dish> getDish() {
        return env -> dishService.findById(Long.parseLong(env.getArgument("id"))).orElseThrow(EntityNotFoundException::new);
    }

    public DataFetcher<Dish> getCreateDish() {
        return env -> {
            DishInput dishInput = converter.convert(env.getArgument("dish"), DishInput.class);
            return dishService.createDish(dishInput);
        };
    }

    public DataFetcher<Dish> getEditDish() {
        return env -> {
            DishInput dishInput = converter.convert(env.getArgument("dishInput"), DishInput.class);
            long dishId = Long.parseLong(env.getArgument("dishId"));
            return dishService.patchDish(dishId, dishInput).orElseThrow(EntityNotFoundException::new);
        };
    }

    public DataFetcher<Boolean> getRemoveDish() {
        return env -> {
            long dishId = Long.parseLong(env.getArgument("dishId"));
            return dishService.deleteDish(dishId);
        };
    }

}
