package com.koenvdels.ordersystem.datafetchFactories;

import com.koenvdels.ordersystem.api.ClientService;
import com.koenvdels.ordersystem.api.OrderService;
import com.koenvdels.ordersystem.api.inputTypes.ClientInput;
import com.koenvdels.ordersystem.api.inputTypes.OrderInput;
import com.koenvdels.ordersystem.api.models.Client;
import com.koenvdels.ordersystem.api.models.Order;
import com.koenvdels.ordersystem.services.GraphQLInputConverter;
import graphql.schema.DataFetcher;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class OrderDataFetcherFactory {

    private final OrderService orderService;
    private final ClientService clientService;
    private final GraphQLInputConverter converter;

    public OrderDataFetcherFactory(OrderService orderService, ClientService clientService, GraphQLInputConverter converter) {
        this.orderService = orderService;
        this.clientService = clientService;
        this.converter = converter;
    }

    public DataFetcher<List<Order>> getAllOrders() {
        return env -> orderService.findAll();
    }

    public DataFetcher<Order> getOrder() {
        return env -> orderService.findOne(Long.parseLong(env.getArgument("id"))).orElse(null);
    }

    public DataFetcher<Order> getCreateOrder() {
        return env -> {
            ClientInput clientInput = converter.convert(env.getArgument("client"), ClientInput.class);
            Client client = clientService.createClient(clientInput);

            OrderInput orderInput = converter.convert(env.getArgument("order"), OrderInput.class);
            orderInput.client = client;

            return orderService.createOrder(orderInput);
        };
    }

    public DataFetcher<Boolean> getRemoveOrder() {
        return env -> {
            long orderId = Long.parseLong(env.getArgument("orderId"));
            return orderService.removeOrder(orderId);
        };
    }
}
