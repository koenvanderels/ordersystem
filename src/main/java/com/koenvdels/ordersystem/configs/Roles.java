package com.koenvdels.ordersystem.configs;

public enum Roles {
    ADMIN,
    USER,
    AUTHENTICATION,
}
