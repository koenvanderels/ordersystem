package com.koenvdels.ordersystem.configs;

import com.koenvdels.ordersystem.datafetchFactories.ClientDataFetcherFactory;
import com.koenvdels.ordersystem.datafetchFactories.DishDataFetcherFactory;
import com.koenvdels.ordersystem.datafetchFactories.OrderDataFetcherFactory;
import com.koenvdels.ordersystem.datafetchFactories.OrderedItemFetcherFactory;
import graphql.schema.idl.RuntimeWiring;
import org.springframework.stereotype.Component;

@Component
public class RuntimeWiringConfig {

    private final DishDataFetcherFactory dishDataFetchers;
    private final OrderDataFetcherFactory orderDataFetcher;
    private final ClientDataFetcherFactory clientDataFetcher;
    private final OrderedItemFetcherFactory orderedItemDataFetcher;

    public RuntimeWiringConfig(DishDataFetcherFactory allDishesDataFetcher, OrderDataFetcherFactory orderDataFetcherFactory, ClientDataFetcherFactory clientDataFetcherFactory, OrderedItemFetcherFactory orderedItemDataFetcher) {
        this.dishDataFetchers = allDishesDataFetcher;
        this.orderDataFetcher = orderDataFetcherFactory;
        this.clientDataFetcher = clientDataFetcherFactory;
        this.orderedItemDataFetcher = orderedItemDataFetcher;
    }

    public RuntimeWiring buildWiring() {
        return RuntimeWiring.newRuntimeWiring()
                .type("Query", builder ->
                        builder.dataFetcher("dishes", dishDataFetchers.getAllDishes())
                                .dataFetcher("dish", dishDataFetchers.getDish())
                                .dataFetcher("order", orderDataFetcher.getOrder())
                                .dataFetcher("orders", orderDataFetcher.getAllOrders())
                                .dataFetcher("client", clientDataFetcher.getClientDataFetcher())
                                .dataFetcher("clients", clientDataFetcher.getAllClientDataFetcher()))
                .type("Mutation", builder ->
                        builder.dataFetcher("createOrder", orderDataFetcher.getCreateOrder())
                                .dataFetcher("createDish", dishDataFetchers.getCreateDish())
                                .dataFetcher("addOrderedItem", orderedItemDataFetcher.getAddOrderedItem())
                                .dataFetcher("editDish", dishDataFetchers.getEditDish())
                                .dataFetcher("editClient", clientDataFetcher.getEditClient())
                                .dataFetcher("editOrderedItem", orderedItemDataFetcher.getEditOrderedItem())
                                .dataFetcher("removeDish", dishDataFetchers.getRemoveDish())
                                .dataFetcher("removeOrder", orderDataFetcher.getRemoveOrder())
                                .dataFetcher("removeClient", clientDataFetcher.getRemoveClient())
                                .dataFetcher("removeOrderedItem", orderedItemDataFetcher.getRemoveOrderedItem())).build();
    }
}
