package com.koenvdels.ordersystem.configs;

import com.koenvdels.ordersystem.dataModels.ClientData;
import com.koenvdels.ordersystem.dataModels.DishData;
import com.koenvdels.ordersystem.dataModels.OrderData;
import com.koenvdels.ordersystem.dataModels.OrderedItemsData;
import com.koenvdels.ordersystem.repositories.ClientRepository;
import com.koenvdels.ordersystem.repositories.DishRepository;
import com.koenvdels.ordersystem.repositories.OrderRepository;
import com.koenvdels.ordersystem.repositories.OrderedItemsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class DatabaseInitializer {

    @Autowired
    private DishRepository dishRepository;
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private OrderedItemsRepository orderedItemsRepository;
    @Autowired
    private ClientRepository clientRepository;

    private int counter;

    @PostConstruct
    public void initDB() {
        System.out.println("Initializing the database");
        List<DishData> dishes = Stream.of(
                new DishData(),
                new DishData(),
                new DishData(),
                new DishData(),
                new DishData()
        ).peek((item) -> {
            item.setName("product" + counter++);
            item.setPrice(counter * 5000);
        }).collect(Collectors.toList());

        List<OrderedItemsData> items = Stream.of(
                new OrderedItemsData(),
                new OrderedItemsData(),
                new OrderedItemsData(),
                new OrderedItemsData()
        ).peek(item -> {
            item.setAmount(counter++);
            DishData dish = dishes.get(randomNumber(1, dishes.size()));
            item.setDishData(dish);
            item.setPrice(dish.getPrice());
        }).collect(Collectors.toList());

        List<OrderData> orders = Stream.of(
                new OrderData(),
                new OrderData(),
                new OrderData(),
                new OrderData()
        ).peek(item -> {
            item.setPaid(new Random().nextBoolean());
        }).collect(Collectors.toList());

        List<ClientData> clients = Stream.of(
                new ClientData(),
                new ClientData(),
                new ClientData(),
                new ClientData()
        ).peek(client -> {
            client.setName("client" + counter++);
            client.setAddress("Somestreet" + counter++);
            client.setEmail("client" + (counter - 2) + "@gmail.com");
            client.setPhoneNumber("0634593058");
        }).collect(Collectors.toList());


        items.forEach((item) -> item.setOrderData(orders.get(1)));
        for (int i = 0; i < clients.size(); i++) {
            clients.get(i).setOrderData(orders.get(i));
        }

        dishes.forEach(dish -> dishRepository.save(dish));
        orders.forEach(order -> orderRepository.save(order));
        items.forEach(item -> orderedItemsRepository.save(item));
        clients.forEach(client -> clientRepository.save(client));
    }

    private int randomNumber(int min, int max) {
        return new Random().nextInt(max - min) + min;
    }
}
