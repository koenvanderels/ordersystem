package com.koenvdels.ordersystem.services;

import com.koenvdels.ordersystem.configs.Roles;

public interface Context {

    boolean hasRole(Roles role);
    boolean hasRole(String[] roles);

}
