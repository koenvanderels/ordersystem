package com.koenvdels.ordersystem.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;

@Service
public class GraphQLInputConverter {

    private ObjectMapper objectMapper;

    public GraphQLInputConverter() {
        objectMapper = new ObjectMapper();
    }

    public <T> T convert(LinkedHashMap object, Class<T> classType) {
        return objectMapper.convertValue(object, classType);
    }
}
