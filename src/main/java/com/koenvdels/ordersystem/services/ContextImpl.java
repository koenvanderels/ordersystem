package com.koenvdels.ordersystem.services;

import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service
public class ContextImpl implements Context {

    private final SecurityContext context;

    public ContextImpl() {
        this.context = SecurityContextHolder.getContext();
    }

    public boolean hasRole(String role) {
        return context.getAuthentication().getAuthorities().stream().anyMatch(o -> o.getAuthority().equals(role));
    }

    @Override
    public boolean hasRole(String[] roles) {
        return context.getAuthentication().getAuthorities().stream()
                .anyMatch(authority -> Arrays.stream(roles).anyMatch(role -> authority.getAuthority().equals(role)));
    }
}
