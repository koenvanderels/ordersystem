package com.koenvdels.ordersystem.services;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import com.koenvdels.ordersystem.configs.RuntimeWiringConfig;
import graphql.GraphQL;
import graphql.schema.GraphQLSchema;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.SchemaGenerator;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.TypeDefinitionRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.net.URL;

@Component
public class GraphQLService {

    @Autowired
    private RuntimeWiringConfig runtimeWiringConfig;

    private GraphQL graphQL;

    @PostConstruct
    private void loadSchema() throws IOException {
        URL url = Resources.getResource("schema.graphql");
        String schema = Resources.toString(url, Charsets.UTF_8);
        TypeDefinitionRegistry typeRegistry = new SchemaParser().parse(schema);
        RuntimeWiring runtimeWiring = runtimeWiringConfig.buildWiring();
        SchemaGenerator schemaGenerator = new SchemaGenerator();
        GraphQLSchema graphQLSchema = schemaGenerator.makeExecutableSchema(typeRegistry, runtimeWiring);
        this.graphQL = GraphQL.newGraphQL(graphQLSchema).build();
    }

    @Bean
    public GraphQL getGraphQL() {
        return graphQL;
    }
}
