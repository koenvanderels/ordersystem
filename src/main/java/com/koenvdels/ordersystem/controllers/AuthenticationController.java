package com.koenvdels.ordersystem.controllers;

import com.koenvdels.ordersystem.dataModels.ApplicationUser;
import com.koenvdels.ordersystem.repositories.ApplicationUserRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AuthenticationController {

    private final ApplicationUserRepository repository;
    private final BCryptPasswordEncoder encoder;

    public AuthenticationController(ApplicationUserRepository repository, BCryptPasswordEncoder encoder) {
        this.repository = repository;
        this.encoder = encoder;
    }

    @PostMapping("createUser")
    public ResponseEntity<?> createUser(@RequestBody ApplicationUser user) {
        user.setPassword(encoder.encode(user.getPassword()));
        repository.save(user);
        return ResponseEntity.ok(user);
    }
}
