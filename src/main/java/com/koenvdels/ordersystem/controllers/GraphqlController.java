package com.koenvdels.ordersystem.controllers;


import com.koenvdels.ordersystem.services.GraphQLService;
import graphql.ExecutionResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GraphqlController {

    private final GraphQLService graphQLService;

    public GraphqlController(GraphQLService graphQLService) {
        this.graphQLService = graphQLService;
    }

    @PostMapping("/graphql")
    public ExecutionResult graphqlEndpoint(@RequestBody String query) {
        return graphQLService.getGraphQL().execute(query);
    }
}