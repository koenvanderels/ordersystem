package com.koenvdels.ordersystem.repositories;

import com.koenvdels.ordersystem.dataModels.ClientData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRepository extends JpaRepository<ClientData, Long> {
}
