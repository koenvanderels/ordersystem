package com.koenvdels.ordersystem.repositories;

import com.koenvdels.ordersystem.dataModels.OrderData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends JpaRepository<OrderData, Long> {
}
