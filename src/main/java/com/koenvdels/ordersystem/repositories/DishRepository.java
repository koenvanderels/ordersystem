package com.koenvdels.ordersystem.repositories;

import com.koenvdels.ordersystem.dataModels.DishData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DishRepository extends JpaRepository<DishData, Long> {
}
