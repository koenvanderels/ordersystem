package com.koenvdels.ordersystem.repositories;

import com.koenvdels.ordersystem.dataModels.OrderedItemsData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderedItemsRepository extends JpaRepository<OrderedItemsData, Long> {
}
